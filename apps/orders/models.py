from django.db import models


class Order(models.Model):
    created_date = models.DateTimeField()
    number = models.IntegerField()


class OrderItem(models.Model):
    amount = models.IntegerField()
    order = models.ForeignKey('orders.Order', on_delete=models.CASCADE, related_name='items')
    product_name = models.CharField(max_length=255)
    product_price = models.DecimalField(max_digits=19, decimal_places=2)
