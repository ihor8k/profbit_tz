from django.urls import path
from apps.orders import views


app_name = 'orders'

urlpatterns = [
    path('analytical-1/', views.Analytical1View.as_view()),
    path('items/top/', views.TopItemsView.as_view()),
]
