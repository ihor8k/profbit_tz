from django.db.models import Sum, OuterRef, Value, QuerySet, F
from django.db.models.functions import Concat, Cast, TruncSecond
from django.db.models import CharField, DateTimeField, DecimalField
from django.views.generic import TemplateView
from apps.orders.forms import DateFilterForm
from apps.orders.models import Order, OrderItem
from apps.orders.query_func import ConcatSubquery


class Analytical1View(TemplateView):
    template_name = 'analytical-1.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        form = DateFilterForm(self.request.GET)
        form.is_valid()
        ctx['data'] = (Order.objects
                       .prefetch_related('items')
                       .filter(created_date__date__gte=form.cleaned_data['from_date'],
                               created_date__date__lte=form.cleaned_data['to_date'])
                       .annotate(total_prices=Sum(F('items__product_price') * F('items__amount'),
                                                  output_field=DecimalField(max_digits=19, decimal_places=2)),
                                 total_amount=Sum('items__amount')))
        ctx['form'] = form
        return ctx


class TopItemsView(TemplateView):
    template_name = 'top-items.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        form = DateFilterForm(self.request.GET)
        form.is_valid()
        concat_orders = Concat(Value('замовлення '), 'order__number', Value(' - ціна '), 'product_price', Value(' - дата '),
                               Cast(TruncSecond('order__created_date', DateTimeField()), CharField()),
                               output_field=CharField())
        sub_query = OrderItem.objects.select_related('order').annotate(orders=concat_orders).filter(product_name=OuterRef('product_name')).values('orders')
        query = (OrderItem.objects
                 .select_related('order')
                 .values('product_name')
                 .filter(order__created_date__date__gte=form.cleaned_data['from_date'],
                         order__created_date__date__lte=form.cleaned_data['to_date'])
                 .annotate(total_amount=Sum('amount'), orders=ConcatSubquery(sub_query))
                 .order_by('-total_amount')[:20].query)
        query.group_by = ['product_name']
        ctx['data'] = QuerySet(query=query, model=OrderItem)
        ctx['form'] = form
        return ctx
