from random import randint
from datetime import timedelta
from django.core.management.base import BaseCommand
from django.db import transaction
from django.conf import settings
from apps.orders import models


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-a', '--amount', type=int, default=1)

    def handle(self, *args, **options):
        amount = options['amount']

        with transaction.atomic():
            orders = models.Order.objects.bulk_create([models.Order(number=i, created_date=(settings.START_DATE + timedelta(hours=i)))
                                                       for i in range(1, amount+1)])
            items = [models.OrderItem(amount=randint(1, 10),
                                      order_id=order.id,
                                      product_name=f'Товар-{randint(1, 100)}',
                                      product_price=randint(100, 9999))
                     for order in orders for j in range(1, (randint(1, 5) + 1))]
            models.OrderItem.objects.bulk_create(items)
