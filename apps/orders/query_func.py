from django.db.models import Subquery, CharField


class ConcatSubquery(Subquery):
    template = 'ARRAY_TO_STRING(ARRAY(%(subquery)s), %(separator)s)'
    output_field = CharField()

    def __init__(self, *args, **kwargs):
        self.separator = kwargs.pop('separator', ', ')
        super().__init__(*args, **kwargs)

    def as_sql(self, compiler, connection, template=None, **extra_context):
        extra_context['separator'] = '%s'
        sql, sql_params = super().as_sql(compiler, connection, template, **extra_context)
        sql_params = sql_params + (self.separator, )
        return sql, sql_params
