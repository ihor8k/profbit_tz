from django.db import connection
from django.utils.deprecation import MiddlewareMixin
from django.conf import settings


class QueryCountMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        if settings.DEBUG:
            request.query_count = len(connection.queries)
        response = self.get_response(request)
        return response
