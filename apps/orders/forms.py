from django import forms
from django.utils.timezone import now
from django.conf import settings


class DateFilterForm(forms.Form):
    from_date = forms.DateField(initial=settings.START_DATE.date().strftime('%Y-%m-%d'), required=False)
    to_date = forms.DateField(initial=now().date().strftime('%Y-%m-%d'), required=False)

    class Meta:
        fields = 'from_date', 'to_date',

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['from_date'] > cleaned_data['to_date']:
            self.add_error('from_date', 'Дата не може бути більшою')
        return cleaned_data

    def clean_from_date(self):
        from_date = self.cleaned_data['from_date']
        if from_date is None:
            return self.fields['from_date'].initial
        return from_date

    def clean_to_date(self):
        to_date = self.cleaned_data['to_date']
        if to_date is None:
            return self.fields['to_date'].initial
        return to_date
