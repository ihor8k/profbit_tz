FROM python:3.9-slim-buster

ENV DJANGO_SETTINGS_MODULE config.settings
ENV ROOT_DIR /usr/src/app

RUN apt-get update && \
    apt-get install -y build-essential && \
    apt-get install -y libpq-dev && \
    apt-get install -y libjpeg-dev && \
    apt-get install -y gettext && \
    apt-get install -y zlib1g-dev && \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    rm -rf /var/lib/apt/lists/*

COPY . $ROOT_DIR

COPY ./requirements.txt /requirements.txt
RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r /requirements.txt && \
    rm -f /requirements.txt

WORKDIR $ROOT_DIR
